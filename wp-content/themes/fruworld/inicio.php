<div class="row fullWidth collapse">
    <div class="small-12 columns">
        <?php echo do_shortcode("[huge_it_slider id='1']"); ?>
    </div>
</div>

<div class="row">
    <div class="small-12 columns">
        <h1 class="titulo verde">Condominio agroecológico Fruworld</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis massa sit 
            amet mollis semper. Donec tortor felis, tempor quis pretium at, varius mattis risus.
            Phasellus sed commodo mauris. Vestibulum eu leo a elit efficitur elementum. Donec 
            nec ultricies eros. Phasellus varius non turpis ac placerat. Cras molestie massa 
            dapibus magna placerat, non fringilla enim placerat.
        </p>
        <p>
            Vivamus non semper justo. Sed consequat felis id nisi mollis, eu dictum metus 
            interdum. Quisque nec ornare magna. Nam volutpat augue quis dapibus maximus. 
            Nulla convallis neque et eleifend tempor. Suspendisse at hendrerit eros. 
            Proin nec nibh mollis, sagittis velit sit amet, rutrum turpis. Integer 
            imperdiet urna aliquam nisl eleifend, nec ultrices augue sollicitudin. 
            Morbi et dapibus tortor, et bibendum felis. Sed dignissim, nisl eget 
            dignissim dapibus, nisl enim euismod libero, fermentum malesuada augue 
            arcu at justo. Quisque euismod eu purus rhoncus imperdiet.
        </p>
    </div>
</div>

<div class="row">
    <div class="medium-6 columns">
        <a href="#">
            <img class="img_fullwidth" src="<?php echo get_template_directory_uri(); ?>/images/lotes.jpg" />
        </a>
    </div>
    <div class="medium-6 columns">
        <a href="#">
            <img class="img_fullwidth" src="<?php echo get_template_directory_uri(); ?>/images/quintas.jpg" />
        </a>
    </div>
</div>