<div class="localizacion">
    <div class="donde">
        <div class="row">
            <div class="small-12 columns text-center">
                <h1 class="titulo blanco">Dónde estamos ubicados</h1>
                <p>A 2 horas de Bogotá, con la doble calzada Bogotá - Girardot</p>
                <p>Hoy estamos a 30 minutos de Melgar y/o a tan solo 10 minutos de Carmen de Apicalá 
                    por la vía a Cunday</p>
            </div>
        </div>
    </div>

    <div class="mapa">
        <div class="row fullWidth collapse">
            <div class="small-12 columns">
                <img src="<?php echo get_template_directory_uri(); ?>/images/mapa.jpg" class="img_fullwidth" />
            </div>
        </div>
    </div>
</div>