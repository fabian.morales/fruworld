<div class="video">
    <div class="row">
        <div class="small-12 columns text-center">
            <div class="boton amarillo">
                Comuníquese: 310 xxx xx xx
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns text-center">
            <br />
            <iframe src="https://player.vimeo.com/video/24253126?portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/24253126">Ocean Sky</a> from <a href="https://vimeo.com/terrastro">Alex Cherney</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
        </div>
    </div>
</div>
