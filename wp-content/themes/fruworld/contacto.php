<div class="contactenos">
    <form id="form_contacto">
        <div class="row">
            <div class="small-12 columns">
                <h1 class="titulo negro text-center">Contáctenos</h1>
                <p>Envíenos sus datos, separe una cita y/o solicite información detallada 
                    de sus necesidades e inquietudes; le responderemos muy pronto</p>
            </div>
        </div>
        <div class="row">
            <div class="medium-4 columns">
                <div class="row collapse">
                    <div class="small-10 columns">
                        <input type="text" name="nombre" placeholder="Nombre completo" />
                    </div>
                    <div class="small-2 columns">
                        <span class="postfix"><i class="fi-torso"></i></span>
                    </div>
                </div>
            </div>
            <div class="medium-4 columns">
                <div class="row collapse">
                    <div class="small-10 columns">
                        <input type="email" name="email" placeholder="Email" />
                    </div>
                    <div class="small-2 columns">
                        <span class="postfix"><i class="fi-mail"></i></span>
                    </div>
                </div>
            </div>
            <div class="medium-4 columns"><input type="text" name="asunto" placeholder="Asunto" /></div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <textarea name="mensaje"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <input type="submit" value="Enviar" id="btnEnviarContacto" />
            </div>
        </div>
    </form>
</div>