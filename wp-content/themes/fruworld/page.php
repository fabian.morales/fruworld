<?php 

get_header(); 
get_page($page_id);
$page_data = get_page($page_id);
?>

<div class="row">
    <div class="small-12 columns">
	<div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
                <h1 class="titulo verde"><?php echo $page_data->post_title; ?></h1>
                <?php echo apply_filters('the_content', $page_data->post_content); ?>
            </div><!-- #content -->
	</div><!-- #primary -->	
    </div>
</div><!-- #main-content -->

<?php
get_footer();
