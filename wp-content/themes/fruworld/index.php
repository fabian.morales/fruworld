<?php get_header(); ?>
<section id="main" class="contenido">

<?php 
if (is_home()){
    get_template_part('inicio');
    get_template_part('galeria_fotos');
    get_template_part('galeria_videos');
    get_template_part('galeria_mapa');
    get_template_part('galeria_video');
    get_template_part('contacto');    
}

/* if (have_posts()) :  while (have_posts()) : the_post(); ?>
<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<small>Publicado el <?php the_time('j/m/Y'); ?> por <?php the_author_posts_link(); ?> </small>
<?php the_excerpt(); ?>
<?php endwhile; else: ?>
<p><?php _e('No hay entradas .'); ?></p>
<?php endif; */ ?>  
</section> <!-- Fin de main -->

<?php /* get_sidebar() */ ?>
<?php get_footer();
