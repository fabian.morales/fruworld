<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <title>Fruworld <?php 
        if (!is_home()){
            wp_title(); 
        }
        ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/foundation.min.css" />
        <link rel="stylesheet" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/foundation-icons.css" />
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
        <link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
            (function(window, $){         
                function scrollTo(target) {
                    var wheight = $(window).height() / 2;

                    var alto = $("#header").hasClass("header_fijo") ? $("#header").height() : $("#header").height() * 2;

                    var ooo = $(target).offset().top - alto;
                    $('html, body').animate({scrollTop:ooo}, 600);
                }


                $(document).ready(function() {
                    $(window).resize(function() {                
                        if ($("#top-menu-resp").is(":visible")){
                            $("header ul").hide();
                        }
                        else{                    
                            $("header ul").show();
                        }
                    });

                    $("#top-menu-resp a").click(function(e){
                        e.preventDefault();
                        $("header ul").toggle("slow");
                    });
                    
                    $("#btnEnviarContacto").click(function(e) {
                        e.preventDefault();

                        if ($("#nombre").val() === ""){
                            alert('Debe ingresar su nombre');
                            return;
                        }

                        if ($("#email").val() === ""){
                            alert('Debe ingresar su dirección de correo');
                            return;
                        }
                        
                        if ($("#asunto").val() === ""){
                            alert('Debe ingresar el asunto');
                            return;
                        }

                        if ($("#mensaje").val() === ""){
                            alert('Debe ingresar un mesaje');
                            return;
                        }

                        $.ajax({
                            url: '/2015/php/contacto.php',
                            data: $("#form_contacto").serialize(),
                            method: 'post',
                            success: function (res){
                                alert(res);
                                $("#form_contacto input[type='text'], #form_contacto textarea, #form_contacto input[type='email']").each(function (i, o) {
                                    $(o).val('');
                                });
                            }
                        });
            });
                });
            })(window, jQuery);
        </script>
        <?php wp_head(); ?>
    </head>
    <body>
        <header>
            <div class="row">
                <div class="medium-8 columns">
                    <div id="top-menu-resp" class="menu-resp">
                        <div>Menu</div>
                        <a href="#"></a>
                    </div>
                    <?php wp_nav_menu( array('menu' => 'Main', 'container' => 'nav' )); ?>
                </div>
                <div class="medium-4 columns">
                    <div class="boton verde">
                        Comuníquese: 310 xxx xx xx
                    </div>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/eua.png" />
                    <img src="<?php echo get_template_directory_uri(); ?>/images/col.png" />
                </div>
            </div>
        </header>
